# FORDCPO Application
## Best Pick


### New Features!

  - Using ASP.Net Core 3.0
  - Running from the Docker in Linux
  - The Database is MySQL from RDS
  - Generate the PDF reports by PDFSharpCore
  - Print the windowsticker by PDF format 
  

You can also:

  - Upload the Banners and PDF files to S3 bucket
  - using Secret Manager to rewrite the configure file
  - Export documents as Markdown, HTML and PDF

### Development Tools

####   Visual Studio 2019, Download it from https://visualstudio.microsoft.com/vs/

####   Install MySQL.Data  (MySQL data connector)
  
>  Tools >> NuGet Package Management >> Manage NuGet Packages for solution
  
>  Install **MySQL.Data** and **MySql.Data.Entity**

####   Install PDFSharpCore  (for PDF generator)

>  Tools >> NuGet Package Management >> Manage NuGet Packages for solution
  
>  Install **PDFSharpCore**

####   Install wsFederation (for ADFS login)

>  Tools >> NuGet Package Management >> Manage NuGet Packages for solution
  
>  Install **Microsoft.AspNetCore.Authentication.WsFederation**

####   Install AWS S3  (for Banners and Pdfs upload to S3 bucket)

>  Tools >> NuGet Package Management >> Manage NuGet Packages for solution
  
>  Install **AWSSDK.Core** and **AWSSDK.S3"


### Configure file

#### Add these configuration values into the Amazon Secret Management

```
{
  "ConnectionStrings": {
    "FORDCPO": "Datatabse connection string"
  },

  "AWSKey": {
    "key": "AKIAWUHRJJXGBUMOFWNF",
    "secret": "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",
    "bucket": "cbbcpo-media/FordCPO"
  },

  "ADFS": {
    "MetadataAddress": "https://corp.sts.ford.com/FederationMetadata/2007-06/FederationMetadata.xml",
    "Wtrealm": "https://fordcpo.canadianblackbook.com",
    "FORDUserID": "m-trenk3",
    "FordUserPassword": "Zm9yZHZpc2kyNA==",
    "WSLLoginURL": "https://www.wslx.dealerconnection.com/auth.cgi"


  },

  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*"
}

```

### Open the project, build and run

> Open the project "FordCPO2020.csproj"
> Click "Docker" and run

### Docker File

```
#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base

RUN apt-get update && apt-get install -y libgdiplus curl wget

# Datadog
RUN curl -LO https://github.com/DataDog/dd-trace-dotnet/releases/download/v1.18.2/datadog-dotnet-apm_1.18.2_amd64.deb
RUN dpkg -i ./datadog-dotnet-apm_1.18.2_amd64.deb

ENV CORECLR_ENABLE_PROFILING=1
ENV CORECLR_PROFILER={846F5F1C-F9AE-4B07-969E-05C26BC060D8}
ENV CORECLR_PROFILER_PATH=/opt/datadog/Datadog.Trace.ClrProfiler.Native.so
ENV DD_INTEGRATIONS=/opt/datadog/integrations.json
ENV DD_DOTNET_TRACER_HOME=/opt/datadog
ENV DD_TRACE_ANALYTICS_ENABLED=true
ENV DD_SERVICE_NAME fordcpo

WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["FordCPO2020.csproj", ""]
RUN dotnet restore "./FordCPO2020.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "FordCPO2020.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "FordCPO2020.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "FordCPO2020.dll"]

```

###End